//
//  RightMenuVC.h
//  SwipeMenuControlDemo
//
//  Created by LeeQuox on 12/18/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "History.h"
@interface RightMenuVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic)BOOL wantsCustomAnimation;
@property(nonatomic,strong)History *history;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@end
