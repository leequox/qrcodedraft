//
//  RightMenuVC.m
//  SwipeMenuControlDemo
//
//  Created by LeeQuox on 12/18/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import "RightMenuVC.h"
#import "History.h"
#import "AppDelegate.h"
#import "DetailRightMenuVC.h"

@interface RightMenuVC ()<NSFetchedResultsControllerDelegate>
@property(strong,nonatomic)NSFetchedResultsController *fetchInfo;
@end

@implementation RightMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark Setup Table
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_fetchInfo.fetchedObjects count] ;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    History *history=_fetchInfo.fetchedObjects[indexPath.row];
    cell.textLabel.text= [NSString stringWithFormat:@"             %@",history.info];
    cell.detailTextLabel.text=[NSString stringWithFormat:@"                 %@",history.date];
    return cell;
}
//---
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        ////        History *history=_fetchInfo.fetchedObjects[indexPath.row];
        ////        NSLog(@"dell79%@",history);
        //        AppDelegate *_appDelegate=[UIApplication sharedApplication].delegate;
        //        // [_appDelegate.managedObjectContext deleteObject:[self.history ]];
        ////        [_appDelegate.managedObjectContext deleteObject:history];
        ////        [_appDelegate saveContext];
        //        History *history=[self.fetchInfo objectAtIndexPath:indexPath];
        //        [_appDelegate.managedObjectContext deleteObject:history];
        //        [_appDelegate saveContext];
        _history=_fetchInfo.fetchedObjects[indexPath.row];
        NSLog(@"dell79%@",_history);
        AppDelegate *_appDelegate=[UIApplication sharedApplication].delegate;
        [_appDelegate.managedObjectContext deleteObject:_history];
        [_appDelegate saveContext];
        [self.tableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        //[self.tableview reloadData];
        
    }
    // [self.tableview reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController=self.revealViewController;
    DetailRightMenuVC *vc=[[DetailRightMenuVC alloc]init];
    vc.history                  = _fetchInfo.fetchedObjects[indexPath.row];
    [revealController pushFrontViewController:vc animated:YES];
}
#pragma mark - CoreData
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
}
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableview reloadData];
}


- (NSFetchedResultsController*)fetchInfo
{
    if (!_fetchInfo) {
        NSString *entityName = @"History";
        AppDelegate *_appDelegate = [UIApplication sharedApplication].delegate;
        
        NSString *cacheName = [NSString stringWithFormat:@"%@",entityName];
        [NSFetchedResultsController deleteCacheWithName:cacheName];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:_appDelegate.managedObjectContext];
        
        
        NSSortDescriptor *sort0 = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
        NSArray *sortList = [NSArray arrayWithObjects:sort0, nil];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"info != nil"];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        fetchRequest.fetchBatchSize = 20;
        fetchRequest.sortDescriptors = sortList;
        fetchRequest.predicate = predicate;
        _fetchInfo = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                            managedObjectContext:_appDelegate.managedObjectContext
                                                              sectionNameKeyPath:nil
                                                                       cacheName:cacheName];
        _fetchInfo.delegate = self;
        
        NSError *error = nil;
        [_fetchInfo performFetch:&error];
        if (error) {
            NSLog(@"%@ core data error: %@", [self class], error.localizedDescription);
        } else {
            NSLog(@"total item : %lu",(unsigned long)[_fetchInfo.fetchedObjects count]);
        }
    }
    
    return _fetchInfo;
}

@end
