//
//  DetailRightMenuVC.m
//  QrCodeDraft
//
//  Created by LeeQuox on 12/19/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "DetailRightMenuVC.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "HomeVC.h"
#import "History.h"
#import "Line.h"
#import "LKLineActivity.h"
#import "OpenInChromeController.h"

@interface DetailRightMenuVC ()<NSFetchedResultsControllerDelegate>
{
    __weak IBOutlet UIButton *btDelete;
    
}
@property(strong,nonatomic)NSFetchedResultsController *fetchInfo;

@end

@implementation DetailRightMenuVC
-(void)presentActivityViewControllerWithItems:(NSArray*)items
{
UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:@[[[LKLineActivity alloc] init]]];
    [self presentViewController:controller animated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btBack:(id)sender {
    SWRevealViewController *revealController=self.revealViewController;
    HomeVC *vc=[[HomeVC alloc]init];
    [revealController pushFrontViewController:vc animated:YES];
}
- (IBAction)btDelete:(id)sender {
    AppDelegate *_appDelegate = [UIApplication sharedApplication].delegate;
    //_history=_
    //_history=_fetchInfo.fetchedObjects[i]
    NSLog(@"79%@",self.history);

    [_appDelegate.managedObjectContext deleteObject:self.history];
    [_appDelegate saveContext];
    //Back to HomeVC
    SWRevealViewController *revealController=self.revealViewController;
    HomeVC *vc=[[HomeVC alloc]init];
    [revealController pushFrontViewController:vc animated:YES];
}
-(void)configView
{
    _lbInfo.text=_history.info;
    NSLog(@"label%@",_history.info);
}
- (IBAction)btOpenLink:(id)sender {
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"http://%@",_history.info]]];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:_history.info ]];

}
- (IBAction)btLine:(id)sender {
    [self presentActivityViewControllerWithItems:@[@"Hello Line!", [NSURL URLWithString:_history.info]]];
    
}
- (IBAction)btChrome:(id)sender {
    NSURL *url=[NSURL URLWithString:_history.info];
    
    [[OpenInChromeController sharedInstance]openInChrome:url];
}

@end
