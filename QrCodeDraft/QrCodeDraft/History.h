//
//  History.h
//  QrCodeDraft
//
//  Created by LeeQuox on 12/19/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface History : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * info;

@end
