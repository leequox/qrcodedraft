//
//  MenuVC.h
//  SwipeMenuControlDemo
//
//  Created by LeeQuox on 12/18/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
