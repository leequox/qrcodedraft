//
//  HomeVC.h
//  SwipeMenuControlDemo
//
//  Created by LeeQuox on 12/18/14.
//  Copyright (c) 2014 LeeQuox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXNavigationController.h"
#import "SlideNavigationController.h"
#import "History.h"
#import <AVFoundation/AVFoundation.h>
@interface HomeVC : UIViewController<AVCaptureMetadataOutputObjectsDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btHistory;
@property(nonatomic,strong)History *history;
@property(strong,nonatomic)NSString *strInfo;
//--
@property (weak, nonatomic) IBOutlet UIButton *btSave;

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UIButton *btStart;

@property (weak, nonatomic) IBOutlet UIButton *bbitemStart;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
- (IBAction)startStopReading:(id)sender;
@end
